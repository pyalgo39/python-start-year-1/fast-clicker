import pygame
import time
from random import randint

pygame.init()

''' Окно игры '''
window = pygame.display.set_mode((500, 500))
back = (200, 255, 255)
window.fill(back)
clock = pygame.time.Clock()

''' Необходимые классы '''
# Класс прямоугольник
class Area():
    def __init__(self, x=0, y=0, width=10, height=10, color=None):
        self.rect = pygame.Rect(x, y, width, height)
        self.fill_color = color

    def color(self, new_color):
        self.fill_color = new_color

    def fill(self):
        pygame.draw.rect(window, self.fill_color, self.rect)
    
    def outline(self, frame_color, thick):
        pygame.draw.rect(window, frame_color, self.rect, thick)
    
    def collidepoint(self, x, y):
        return self.rect.collidepoint(x, y)

# Класс прямоугольник с надписью
class Label(Area):
    def set_text(self, text, fsize=12, text_color=(0, 0, 0)):
        self.image = pygame.font.SysFont('verdana', fsize).render(text, True, text_color)

    def draw(self, shift_x=0, shift_y=0):
        self.fill()
        window.blit(self.image, (self.rect.x + shift_x, self.rect.y + shift_y))

# необходимые цвета
YELLOW = (255, 255, 0)
DARK_BLUE = (0, 0, 100)
BLUE = (80, 80, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 51)

# создание карточек
cards = []
num_cards = 4
x = 70 # координата первой карточки

for i in range(num_cards):
    card = Label(x, 170, 70, 100, YELLOW)
    card.outline(BLUE, 10)
    card.set_text('CLICK', 70)
    cards.append(card)
    x += 100

wait = 0 # счетчик кадров
# необходим для смены надписи CLICK

# игровой цикл
while True:
    # если вышло время для надписи CLICK
    if wait == 0:
        wait = 20
        click = randint(0, num_cards - 1)
        for i in range(num_cards):
            cards[i].color(YELLOW)
            if i == click:
                cards[i].draw(10, 40)    
            else:
                cards[i].fill()
    else:
        wait -= 1
    
    # проверка событий-нажатий на карточки
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            x, y = event.pos
            for i in range(num_cards):
                if cards[i].collidepoint(x, y):
                    if i == click:
                        cards[i].color(GREEN)
                    else:
                        cards[i].color(RED)
                    cards[i].fill()
    pygame.display.update()
    clock.tick(40)

